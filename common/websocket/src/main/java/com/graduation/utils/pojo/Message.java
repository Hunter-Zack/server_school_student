package com.graduation.utils.pojo;

import lombok.Data;


@Data
public class Message {
    private String toName;
    private String message;
}
