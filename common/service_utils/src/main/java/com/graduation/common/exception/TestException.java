package com.graduation.common.exception;

import lombok.Data;

@Data
public class TestException extends RuntimeException {
    private Integer code;
    private String message;


    public TestException(String message) {
        super(message);
        this.message = message;
    }

    public TestException(Integer code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }
}
