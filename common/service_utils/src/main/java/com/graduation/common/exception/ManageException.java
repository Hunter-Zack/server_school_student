package com.graduation.common.exception;

import lombok.Data;

@Data
public class ManageException extends RuntimeException {
    private Integer code;


    public ManageException(String message) {
        super(message);
    }

    public ManageException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    @Override
    public String toString() {
        return "ManageException{" +
                "code=" + code +
                ", message=" + this.getMessage() +
                '}';
    }

}
