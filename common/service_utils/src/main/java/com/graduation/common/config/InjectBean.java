 /*
  * Copyright (c) 2006, 2023, wuyahan 编写
  *
  */
 package com.graduation.common.config;

 import org.springframework.context.annotation.Bean;
 import org.springframework.context.annotation.Configuration;

 import java.util.Map;
 import java.util.concurrent.ConcurrentHashMap;

 /**
  * <p>Project: school end - InjectBean
  * <p>Powered by wuyahan On 2023-03-22 16:10:04
  *
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since 17
  */
 @Configuration
 public class InjectBean {
     /**
      * 作为容器存储用户的登录信息
      * @return
      */
     @Bean("myMap")
     public Map myMap(){
         return new ConcurrentHashMap();
     }

 }
