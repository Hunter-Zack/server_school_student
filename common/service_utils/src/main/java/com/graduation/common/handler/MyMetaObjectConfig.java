 /*
  * Copyright (c) 2006, 2023, wuyahan 编写
  *
  */
 package com.graduation.common.handler;

 import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
 import org.apache.ibatis.reflection.MetaObject;
 import org.springframework.stereotype.Component;

 import java.util.Date;

 /**
  * <p>Project: school end - MyMetaObjectConfig
  * <p>Powered by wuyahan On 2023-02-18 15:15:33
  *
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since 17
  */
 @Component
 public class MyMetaObjectConfig implements MetaObjectHandler {
     @Override
     public void insertFill(MetaObject metaObject) {

        this.setFieldValByName("createTime",new Date(),metaObject);
        this.setFieldValByName("updateTime",new Date(),metaObject);
     }

     @Override
     public void updateFill(MetaObject metaObject) {

        this.setFieldValByName("updateTime",new Date(),metaObject);
     }
 }
