package com.graduation.common.handler;

import com.graduation.common.exception.ManageException;
import com.graduation.common.exception.TestException;
import com.graduation.common.result.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice // 全局异常处理
@Slf4j
public class MyGlobalExceptionHandler {

    @ExceptionHandler(value = TestException.class)
    public R testException(TestException ex) {
        ex.printStackTrace();
        return R.error().message(ex.getMessage());
    }

    @ExceptionHandler(value = ManageException.class)
    public R mangeException(ManageException ex) {
        ex.printStackTrace();
        return R.error().message(ex.getMessage());
    }

    @ExceptionHandler(value = Exception.class)
    public R handlerEx(Exception ex) {
        ex.printStackTrace();
        return R.error().message(ex.getMessage());
    }


}
