 /*
  * Copyright (c) 2006, 2023, wuyahan 编写
  *
  */
 package com.graduation.rabbit.config;

 /**
  * <p>Project: school end - MQConst
  * <p>Powered by wuyahan On 2023-02-18 16:43:32
  *
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since 17
  */
 public class MQConst {

     /**
      *  改变学生的实习信息
      */
     // 配置交换机名称
     public static final String EXCHANGE_DIRECT_STUDENT = "exchange_direct_student";
     // 配置路由信息
     public static final String ROUTING_STUDENT = "student";
     // 配置队列名称
     public static final String QUEUE_STUDENT = "queue.student";

     /**
      * 举报文章
      */
      // 配置交换机名称
     public static final String EXCHANGE_DIRECT_EMAIL = "exchange_direct_email";
     // 配置路由信息
     public static final String ROUTING_EMAIL = "email";
     // 配置队列名称
     public static final String QUEUE_EMAIL = "queue.email";


 }
