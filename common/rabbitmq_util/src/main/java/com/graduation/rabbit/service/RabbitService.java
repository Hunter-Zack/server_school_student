 /*
  * Copyright (c) 2006, 2023, wuyahan 编写
  *
  */
 package com.graduation.rabbit.service;

 import org.springframework.amqp.AmqpException;
 import org.springframework.amqp.rabbit.core.RabbitTemplate;
 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.stereotype.Service;

 /**
  * <p>Project: school end - RabbitService
  * <p>Powered by wuyahan On 2023-02-18 16:48:06
  *
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since 17
  */
 @Service
 public class RabbitService {

     @Autowired
     private RabbitTemplate rabbitTemplate;

     public boolean save(String exchange, String routing, Object message){

         try {
             rabbitTemplate.convertAndSend(exchange,routing,message);
             return true;
         } catch (Exception e) {
             e.printStackTrace();
         }
         return false;
     }



 }
