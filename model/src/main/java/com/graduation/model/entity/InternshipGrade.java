package com.graduation.model.entity;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author tianwenle
 * @since 2022-11-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class InternshipGrade extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 日志编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long logId;

    /**
     * 评分
     */
    private String grade;

    /**
     * 0--正常 1--删除
     */
    private Integer deleted;

}
