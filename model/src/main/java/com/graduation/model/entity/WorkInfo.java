package com.graduation.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author tianwenle
 * @since 2023-02-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WorkInfo extends BaseEntity implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * 用户编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    /**
     * 企业编号
     */
    private String enterpriseCode;

    /**
     * 工号
     */
    private String workCode;

    /**
     * 实习开始时间
     */
    private LocalDate workStartTime;

    /**
     * 实习结束时间
     */
    private LocalDate workEndTime;

    private String position;



}
