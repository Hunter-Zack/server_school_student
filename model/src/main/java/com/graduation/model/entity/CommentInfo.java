package com.graduation.model.entity;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author tianwenle
 * @since 2022-11-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CommentInfo extends BaseEntity implements Serializable {

    private static final long serialVersionUID=1L;



    /**
     * 文章id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long articelId;

    /**
     * 用户编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    /**
     * 评论内容
     */
    private String commentContent;

    /**
     * 点赞数
     */
    private Integer agree;

    /**
     * 文章的状态 0正常 1锁定 2冻结评论
     */
    private Boolean status;

    /**
     * 0--正常 1--删除
     */
    private Integer deleted;




}
