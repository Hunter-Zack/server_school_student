package com.graduation.model.entity;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author tianwenle
 * @since 2022-11-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class InternshipLog extends BaseEntity implements Serializable {

    private static final long serialVersionUID=1L;



    /**
     * 学生id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long studentId;

    /**
     * 教师编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long teacherId;

    /**
     * 0--已批改 1--已退回
     */
    private Integer status;
    /**
     * 评论内容
     */
    private String comment;

    /**
     * 文件名称
     */
    private String fileName;

    /**
     * 文件下载链接
     */
    private String fileUrl;

    @TableField(exist = false)
    private String studentName;

    @TableField(exist = false)
    private String statusString;

}
