package com.graduation.model.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Map;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author tianwenle
 * @since 2022-11-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Student extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 新增登录账号
     */

    private String loginNo;

    /**
     * 新增手机号
     */
    private String phone;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 昵称
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 头像地址
     */
    private String headUrl;

//    /**
//     * 班级id
//     */
//    private Long classId;

    /**
     * 学院id
     */
    private Long departmentId;

    private Long year;

    private Long className;

    private Long classNo;

    /**
     * 教师编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long teacherId;

    private Integer workStatus;

    @TableField(exist = false)
    private String departmentName;

    @TableField(exist = false)
    private String fullInfo;
    @TableField(exist = false)
    private boolean isLog;

    @TableField(exist = false)
    private InternshipGrade internshipGrade;

    @TableField(exist = false)
    private InternshipLog internshipLog;

    @TableField(exist = false)
    private String fileName;

    @TableField(exist = false)
    private String fileUrl;

    @TableField(exist = false)
    private String workStatusStr;

    @TableField(exist = false)
    private Map<String, String> stringInfo;


}
