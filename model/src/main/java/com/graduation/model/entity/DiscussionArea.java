package com.graduation.model.entity;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author tianwenle
 * @since 2022-11-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class DiscussionArea extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 讨论内容
     */
    private String content;

    /**
     * 标题
     */
    private String title;


    /**
     * 用户编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    @TableField(exist = false)
    private String username;

    /**
     * 浏览量
     */
    private Integer view;

    /**
     * 点赞数
     */
    private Integer agree;

    /**
     * 0--正常 1--锁定
     */
    private Integer status;


}
