package com.graduation.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 企业信息表
 * </p>
 *
 * @author tianwenle
 * @since 2023-02-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Enterprise extends BaseEntity implements Serializable {

    private static final long serialVersionUID=1L;


    /**
     * 名称
     */
    private String name;

    /**
     * 类型 在字典表中添加
     */
    private String orgtype;

    /**
     * 统一社会信用代码
     */
    private String code;

    /**
     * 备注
     */
    private String memo;

    @TableField(exist = false)
    private String envStr;



}
