package com.graduation.model.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author tianwenle
 * @since 2022-11-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ClassInfo extends BaseEntity implements Serializable {

    private static final long serialVersionUID=1L;



    /**
     * 辅导员名称
     */
    private String instructorName;

    /**
     * 辅导员手机号
     */
    private String instructorPhone;

    /**
     * 班级名称
     */
    private String className;

    /**
     * 学院id
     */
    private Long departmentId;




}
