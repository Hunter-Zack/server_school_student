package com.graduation.model.enu;

public enum LogEnum {

    MARKING_YES(1,"已查阅"),
    MARKING_NO(0,"未查阅"),
    MARKING_BACK(-1,"已打回"),
    ;

    private Integer status;
    private String  value;

    LogEnum(Integer status, String value) {
        this.status = status;
        this.value = value;
    }

    public static String getValueByStatus(Integer status){
        LogEnum[] values = LogEnum.values();
        for (LogEnum value : values) {
            if (status == value.getStatus()){
                return value.getValue();
            }
        }
        return null;
    }

    LogEnum() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
