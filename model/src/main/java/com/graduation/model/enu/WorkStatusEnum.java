package com.graduation.model.enu;

import io.swagger.models.auth.In;

import java.util.Objects;

/**
 * <p>Project: manage - WorkStatusEnum
 * <p>Powered by wuyahan On 2023-02-16 21:02:06
 *
 * @author wuyahan [tianwenle2000@163.com]
 * @version 1.0
 * @since 17
 */
public enum WorkStatusEnum {
    NO_WORK(0,"未实习"),
    WORKING(1,"实习中"),
    WORK_END(2,"实习完毕")
    ;
    private Integer workStatus;

    private String workStr;

    WorkStatusEnum() {
    }


    public static String getWorkStr(Integer workStatus){
        // 获取所有的枚举类
        WorkStatusEnum[] enums = WorkStatusEnum.values();

        // 判断需要的类型
        for (WorkStatusEnum workStatusEnum : enums) {
            if (Objects.equals(workStatusEnum.workStatus, workStatus)){
                return workStatusEnum.workStr;
            }
        }

        return "";
    }


    WorkStatusEnum(Integer workStatus, String workStr) {
        this.workStatus = workStatus;
        this.workStr = workStr;
    }


    public Integer getWorkStatus() {
        return workStatus;
    }

    public void setWorkStatus(Integer workStatus) {
        this.workStatus = workStatus;
    }

    public String getWorkStr() {
        return workStr;
    }

    public void setWorkStr(String workStr) {
        this.workStr = workStr;
    }
}
