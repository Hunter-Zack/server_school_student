 /*
  * Copyright (c) 2006, 2023, wuyahan 编写
  *
  */
 package com.graduation.model.vo;

 import lombok.AllArgsConstructor;
 import lombok.Data;
 import lombok.NoArgsConstructor;

 /**
  * <p>Project: school end - ReportDisscussVo
  * <p>Powered by wuyahan On 2023-04-02 18:55:59
  *
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since 17
  */
 @Data
 @AllArgsConstructor
 @NoArgsConstructor
 public class ReportDiscussVo {

     // 要举报话题的地址
     private String url;
     // 用户举报说明
     private String content;



 }
