package com.graduation.model.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
public class StudentExVo {

    @ExcelProperty(index = 0,value = "登录账号")
    private String loginNo;

    @ExcelProperty(index = 1,value = "学生姓名")
    private String username;

}
