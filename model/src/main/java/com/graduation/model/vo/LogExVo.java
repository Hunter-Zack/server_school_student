 /*
  * Copyright (c) 2006, 2023, wuyahan 编写
  *
  */
 package com.graduation.model.vo;

 import com.alibaba.excel.annotation.ExcelProperty;
 import com.alibaba.excel.annotation.format.DateTimeFormat;
 import com.alibaba.excel.annotation.write.style.ColumnWidth;
 import com.baomidou.mybatisplus.annotation.TableField;
 import lombok.Data;

 import java.time.LocalDate;

 /**
  * <p>Project: school end - LogExVo
  * <p>Powered by wuyahan On 2023-05-04 10:27:49
  *
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since 17
  */
 @Data
 public class LogExVo {

     /**
      * 学生实习信息属性
      */

     @ExcelProperty(value = "学生学号", index = 0)
     private String loginNo;

     @ExcelProperty(value = "学生姓名", index = 1)
     private String username;

     @ExcelProperty(value = "学生手机号", index = 2)
     private String phone;

     @ExcelProperty(value = "学生班级信息", index = 3)
     private String fullInfo;

     @ExcelProperty(value = "学生实习状态", index = 4)
     private String workStatus;

     /**
      * 工号
      */
     @ExcelProperty(value = "学生工作工号", index = 5)
     private String workCode;

     /**
      * 实习开始时间
      */
     @ExcelProperty(value = "学生实习开始时间", index = 6)
     private String workStartTime;

     /**
      * 实习结束时间
      */
     @ExcelProperty(value = "学生实习结束时间", index = 7)
     private String workEndTime;

     /**
      * 指导老师信息属性
      */
     @ExcelProperty(value = "指导老师姓名", index = 8)
     private String teacherName;

     @ExcelProperty(value = "指导老师手机号", index = 9)
     private String teacherPhone;


     @ExcelProperty(value = "指导老师邮箱", index = 10)
     private String teacherEmail;


     /**
      * 企业信息
      */
     @ExcelProperty(value = "企业名称", index = 11)
     private String name;

     /**
      * 类型 在字典表中添加
      */
     @ExcelProperty(value = "企业类型", index = 12)
     private String orgtype;

     /**
      * 统一社会信用代码
      */
     @ExcelProperty(value = "企业信用编码", index = 13)
     private String code;

     /**
      * 备注
      */
     @ExcelProperty(value = "学生备注信息", index = 14)
     private String memo;


 }
