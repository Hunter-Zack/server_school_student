package com.graduation.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

@Data
public class ExcelContentVo {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long teacherId;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long departmentId;

}
