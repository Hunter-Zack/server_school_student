 /*
  * Copyright (c) 2006, 2023, wuyahan 编写
  *
  */
 package com.graduation.model.vo;

 import com.fasterxml.jackson.databind.annotation.JsonSerialize;
 import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
 import lombok.AllArgsConstructor;
 import lombok.Data;
 import lombok.NoArgsConstructor;

 /**
  * <p>Project: school end - StudentWorkVo
  * <p>Powered by wuyahan On 2023-02-18 17:16:05
  *
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since 17
  */
 @Data
 @AllArgsConstructor
 @NoArgsConstructor
 public class StudentWorkVo {

     @JsonSerialize(using = ToStringSerializer.class)
     private Long id;

     private boolean isWork;

 }
