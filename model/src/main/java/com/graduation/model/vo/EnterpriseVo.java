package com.graduation.model.vo;

import lombok.Data;

@Data
public class EnterpriseVo {

    private String value;
    private String label;
}
