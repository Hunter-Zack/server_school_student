package com.graduation.model.vo;

import lombok.Data;

@Data
public class StudentQueryVo {
    /**
     * 昵称
     */
    private String username;

    private Long year;

    private Long className;

    private Long classNo;

    private Long departmentId;
}
