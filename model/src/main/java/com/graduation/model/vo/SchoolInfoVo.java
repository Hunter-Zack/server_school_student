package com.graduation.model.vo;

import lombok.Data;

import java.util.List;

@Data
public class SchoolInfoVo {

    private String value;
    private String label;
}
