 /*
  * Copyright (c) 2006, 2023, wuyahan 编写
  *
  */
 package com.graduation.model.vo;

 import lombok.AllArgsConstructor;
 import lombok.Data;
 import lombok.NoArgsConstructor;

 import java.util.List;

 /**
  * <p>Project: school end - WorkInfoTempVo
  * <p>Powered by wuyahan On 2023-02-18 15:36:12
  *
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since 17
  */
 @Data
 @AllArgsConstructor
 @NoArgsConstructor
 public class WorkInfoTempVo {

     private String workCode;

     private List<String> time;

     private String code;

     private String position;


 }
