package com.graduation.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

@Data
public class GradeQueryVo {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long studentId;

    private Integer status;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long teacherId;

}
