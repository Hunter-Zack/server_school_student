package com.graduation.model.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

@Data
public class LogAndGradeVo {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long studentId;

    private Integer status;

    private String grade;

    @TableField(exist = false)
    private String studentName;

    @TableField(exist = false)
    private String authString;

}
