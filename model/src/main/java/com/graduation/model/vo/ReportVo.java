 /*
  * Copyright (c) 2006, 2023, wuyahan 编写
  *
  */
 package com.graduation.model.vo;

 import com.fasterxml.jackson.databind.annotation.JsonSerialize;
 import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
 import lombok.AllArgsConstructor;
 import lombok.Data;
 import lombok.NoArgsConstructor;

 /**
  * <p>Project: school end - ReportVo
  * <p>Powered by wuyahan On 2023-04-02 19:13:21
  *
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since 17
  */
 @Data
 @AllArgsConstructor
 @NoArgsConstructor
 public class ReportVo {
     // 要举报的文章id
     @JsonSerialize(using = ToStringSerializer.class)
     private Long id;
     // 用户举报说明
     private String content;

 }
