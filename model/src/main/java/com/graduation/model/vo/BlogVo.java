 /*
  * Copyright (c) 2006, 2023, wuyahan 编写
  *
  */
 package com.graduation.model.vo;

 import lombok.AllArgsConstructor;
 import lombok.Data;
 import lombok.NoArgsConstructor;

 /**
  * <p>Project: school end - BlogVo
  * <p>Powered by wuyahan On 2023-03-08 11:33:09
  *
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since 17
  */
 @Data
 @AllArgsConstructor
 @NoArgsConstructor
 public class BlogVo {
    /*
        文章标题
     */
    private String title;
    /*
        文章内容
     */
    private String content;
 }
