package com.graduation.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

@Data
public class TeacherQueryVo {
    // 学院id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long departmentId;
    // 姓名
    private String username;
}
