package com.graduation.user;

import com.graduation.common.handler.MyGlobalExceptionHandler;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.graduation"})
@MapperScan("com.graduation.user.mapper")
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.graduation")
public class UserApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(UserApplication.class, args);
        MyGlobalExceptionHandler bean = context.getBean(MyGlobalExceptionHandler.class);
        System.out.println("bean = " + bean);

    }
}
