 /*
  * Copyright (c) 2006, 2023, wuyahan 编写
  *
  */
 package com.graduation.user.controller;

 import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
 import com.graduation.common.result.R;
 import com.graduation.common.utils.JwtHelper;
 import com.graduation.model.entity.Student;
 import com.graduation.model.entity.Teacher;
 import com.graduation.model.entity.User;
 import com.graduation.user.constant.MapConstant;
 import com.graduation.user.service.StudentService;
 import com.graduation.user.service.TeacherService;
 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.web.bind.annotation.GetMapping;
 import org.springframework.web.bind.annotation.RequestMapping;
 import org.springframework.web.bind.annotation.RestController;

 import javax.servlet.http.HttpServletRequest;
 import java.util.Map;

 /**
  * <p>Project: school end - BaseController
  * <p>Powered by wuyahan On 2023-03-22 18:13:12
  *
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since 17
  */
 @RestController("/admin/user/base")
 public class BaseController {

     @Autowired
     private StudentService studentService;

     @Autowired
     private TeacherService teacherService;


     @GetMapping("/getMap")
     public Map getCommonMap() {
         return MapConstant.MAP;
     }

     @GetMapping("/getCurrentUserInfo")
     public R getCurrentUserInfo(HttpServletRequest request) {

         String token = request.getHeader("X-Token");
         // 获取登录账号 查询用户的姓名
         String loginNo = JwtHelper.getLoginNo(token);

         QueryWrapper<Student> wrapper = new QueryWrapper<>();
         wrapper.eq("login_no", loginNo);

         Student student = studentService.getOne(wrapper);

         if (student == null) {
             // 如果学生信息为空 那就判断是否为教师
             QueryWrapper<Teacher> teacherQueryWrapper = new QueryWrapper<>();
             teacherQueryWrapper.eq("login_no", loginNo);
             Teacher teacher = teacherService.getOne(teacherQueryWrapper);
             if (teacher != null) {
                 return R.ok().data("curUser", teacher);
             }
         } else {

             return R.ok().data("curUser", student);
         }
         return R.ok().data("name", "NULL");
     }

 }
