package com.graduation.user.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.graduation.model.entity.Student;
import com.graduation.model.vo.SchoolInfoVo;
import com.graduation.model.vo.StudentQueryVo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tianwenle
 * @since 2022-11-16
 */
public interface StudentService extends IService<Student> {

    Student getStudentById(Long id);

    List<Student> getListInfo();

    Map<String, Object> findPageList(Integer pageSize, Integer pageNum, StudentQueryVo studentQueryVo);

    List<SchoolInfoVo> getTeacherStudentList(String username);

    Map<String, Object> pageMyStudent(Integer pageNum, Integer pageSize, StudentQueryVo studentQueryVo, String username);

    Student getStudentInfoDetail(String username);

    List<SchoolInfoVo> getPackageStudentInfo();

    void saveAndReload(Student student);

    void addAndChangeInfo(Student student);



}
