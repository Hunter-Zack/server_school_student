package com.graduation.user.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.graduation.client.CmnClient;
import com.graduation.model.entity.Dict;
import com.graduation.model.entity.Student;
import com.graduation.model.entity.Teacher;
import com.graduation.model.enu.WorkStatusEnum;
import com.graduation.user.mapper.AdministratorMapper;
import com.graduation.model.entity.Administrator;
import com.graduation.user.service.AdministratorService;
import com.graduation.user.service.StudentService;
import com.graduation.user.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tianwenle
 * @since 2022-11-16
 */
@Service
public class AdministratorServiceImpl extends ServiceImpl<AdministratorMapper, Administrator> implements AdministratorService {


    @Autowired
    private TeacherService teacherService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private CmnClient cmnClient;

    @Override
    public Map<String, Object> getCountInfo() {

        // ======
        // 统计所有的教师信息
        Map<String, Object> teacherMap = new HashMap<>();
        teacherMap.put("name", "教师人数");
        int count = teacherService.count();
        teacherMap.put("count", count);
        // 统计所有的学生信息
        Map<String, Object> studentMap = new HashMap<>();
        studentMap.put("name", "学生人数");
        int studentCount = studentService.count();
        studentMap.put("count", studentCount);
        // 不创建新的类的 直接使用map集合
        ArrayList<Map> list = new ArrayList<>();
        list.add(teacherMap);
        list.add(studentMap);


        // ====== 获取每个学院的情况
        // 获取每个学院的信息
        List<Dict> allDataType = cmnClient.getAllDataType(666L);
        // 获取所有教师的信息
        List<Teacher> teacherList = teacherService.list();
        // 获取所有学生的信息
        List<Student> studentList = studentService.list();

        // 存储每个院校信息 的情况
        ArrayList<Map> tableData = new ArrayList<>();
        for (Dict dict : allDataType) {

            Map depMap = new HashMap();

            // 获取每个学院的id
            Long depId = dict.getId();
            String depName = dict.getName();
            // 存储院校信息
            depMap.put("name", depName);

            // 根据学院的id 区分出学生的信息
            List<Student> students = studentList.stream().filter(s ->
                    s.getDepartmentId() == depId
            ).collect(Collectors.toList());
            // 根据学院的id 区分出老师的信息
            List<Teacher> teachers = teacherList.stream().filter(t -> t.getDepartmentId() == depId)
                    .collect(Collectors.toList());
            // 存储当前学院的学生数量
            depMap.put("studentCount", students.size());
            // 存储当前学院的教师数量
            depMap.put("teacherCount", teachers.size());
            // 存储已参加实习的学生
            depMap.put("workedCount",
                    (int) students.stream()
                            .filter(s -> Objects.equals(s.getWorkStatus(), WorkStatusEnum.WORK_END.getWorkStatus())).count()
            );

            // 存储实习中的学生
            depMap.put("working",
                    (int) students.stream()
                            .filter(s -> Objects.equals(s.getWorkStatus(), WorkStatusEnum.WORKING.getWorkStatus())).count()
            );

            //  存储未参加实习的学生
            depMap.put("unWork", students.stream().filter(s -> Objects.equals(s.getWorkStatus(), WorkStatusEnum.NO_WORK.getWorkStatus())).count());

            tableData.add(depMap);


        }


        // 存储echarts中的信息
        /**
         * {value: 1048, name: '未参加实习学生'}
         */
        List<Map> echartsData = new ArrayList<>();
        // 参加工作学生人数
        long worked = studentList.stream().filter(s -> Objects.equals(s.getWorkStatus(), WorkStatusEnum.WORK_END.getWorkStatus())).count();
        // 工作中学生人数
        long working = studentList.stream().filter(s -> Objects.equals(s.getWorkStatus(), WorkStatusEnum.WORKING.getWorkStatus())).count();
        // 未参加工作学生人数
        long unWork = studentList.stream().filter(s -> Objects.equals(s.getWorkStatus(), WorkStatusEnum.NO_WORK.getWorkStatus())).count();
        //
        HashMap workedMap = new HashMap<>();
        workedMap.put("name", "实习完毕学生人数");
        workedMap.put("value", worked);
        HashMap workingMap = new HashMap<>();
        workingMap.put("name", "实习中学生人数");
        workingMap.put("value", working);
        HashMap unWorkMap = new HashMap<>();
        unWorkMap.put("name", "未参加实习学生人数");
        unWorkMap.put("value", unWork);

        echartsData.add(workedMap);
        echartsData.add(workingMap);
        echartsData.add(unWorkMap);


        Map<String, Object> dataAll = new HashMap<>();


        dataAll.put("list", list);

        dataAll.put("tableData", tableData);

        dataAll.put("echartsData", echartsData);


        return dataAll;
    }
}
