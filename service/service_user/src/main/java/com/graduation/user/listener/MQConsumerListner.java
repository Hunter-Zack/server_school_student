 /*
  * Copyright (c) 2006, 2023, wuyahan 编写
  *
  */
 package com.graduation.user.listener;

 import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
 import com.graduation.model.entity.Student;
 import com.graduation.model.vo.StudentWorkVo;
 import com.graduation.rabbit.config.MQConst;
 import com.graduation.user.service.StudentService;
 import org.springframework.amqp.rabbit.annotation.Exchange;
 import org.springframework.amqp.rabbit.annotation.Queue;
 import org.springframework.amqp.rabbit.annotation.QueueBinding;
 import org.springframework.amqp.rabbit.annotation.RabbitListener;
 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.stereotype.Component;

 /**
  * <p>Project: school end - MQConsumerListner
  * <p>Powered by wuyahan On 2023-02-18 17:18:55
  *
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since 17
  */
 @Component
 public class MQConsumerListner {

     @Autowired
     private StudentService studentService;


     @RabbitListener(
             bindings = @QueueBinding(
                     value = @Queue(MQConst.QUEUE_STUDENT),
                     exchange = @Exchange(MQConst.EXCHANGE_DIRECT_STUDENT),
                     key = MQConst.ROUTING_STUDENT
             )
     )
     public void consumer(StudentWorkVo studentWorkVo){

         // 获取数据
         Long id = studentWorkVo.getId();
         boolean b = studentWorkVo.isWork();
         // 保险起见 判断数据
         if (b) {
             // 设置学生的状态为已实习
             UpdateWrapper<Student> wrapper = new UpdateWrapper<>();
             wrapper.eq("id",id);
             wrapper.set("work_status",2);
             studentService.update(wrapper);
         }
     }


 }
