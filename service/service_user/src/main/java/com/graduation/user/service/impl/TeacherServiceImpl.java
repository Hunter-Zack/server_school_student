package com.graduation.user.service.impl;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.graduation.client.CmnClient;
import com.graduation.model.entity.Dict;
import com.graduation.model.entity.Teacher;
import com.graduation.model.vo.ExcelContentVo;
import com.graduation.model.vo.SchoolInfoVo;
import com.graduation.model.vo.StudentExVo;
import com.graduation.model.vo.TeacherQueryVo;
import com.graduation.user.listener.MyAddStuListener;
import com.graduation.user.mapper.StudentMapper;
import com.graduation.user.mapper.TeacherMapper;
import com.graduation.user.service.TeacherService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tianwenle
 * @since 2022-11-16
 */
@Service
public class TeacherServiceImpl extends ServiceImpl<TeacherMapper, Teacher> implements TeacherService {

    @Autowired
    private CmnClient cmnClient;

    @Autowired
    private StudentMapper studentMapper;

    @Override
    public Teacher getTeacherInfoById(Long id) {
        Teacher teacher = baseMapper.selectById(id);
        this.packageTeacherInfo(teacher);
        return teacher;
    }

    @Override
    public List<Teacher> getTeacherList() {
        List<Teacher> list = baseMapper.selectList(null);
        for (Teacher teacher : list) {
            this.packageTeacherInfo(teacher);
        }
        return list;
    }

    @Override
    public Map<String,Object> teacherPage(Integer pageSize, Integer limit, TeacherQueryVo teacherQueryVo) {
        Page<Teacher> page = new Page<>(limit,pageSize);
        QueryWrapper<Teacher> wrapper = new QueryWrapper<>();
        if (teacherQueryVo.getDepartmentId() != null){
            wrapper.eq("department_id",teacherQueryVo.getDepartmentId());
        }
        if (!StringUtils.isEmpty(teacherQueryVo.getUsername())){
            wrapper.like("username",teacherQueryVo.getUsername());
        }
        baseMapper.selectPage(page,wrapper);
        Map<String,Object> map = new HashMap();
        map.put("total",page.getTotal());
        List<Teacher> teacherList = page.getRecords();
        teacherList.forEach(this::packageTeacherInfo);
        map.put("list", teacherList);
        return map;
    }

    @Override
    public List<SchoolInfoVo> getTeacherInfo() {
        List<Teacher> list = baseMapper.selectList(null);
        List<SchoolInfoVo> schoolInfoVos = new ArrayList<>();
        for (Teacher teacher : list) {
            SchoolInfoVo schoolInfoVo = new SchoolInfoVo();
            schoolInfoVo.setLabel(teacher.getUsername());
            schoolInfoVo.setValue(teacher.getId().toString());
            schoolInfoVos.add(schoolInfoVo);
        }
        return schoolInfoVos;
    }



    @Override
    public void batchAddStudents(MultipartFile file, ExcelContentVo excelContentVo) throws IOException {
        EasyExcel.read(file.getInputStream(), StudentExVo.class,new MyAddStuListener(studentMapper,excelContentVo)).sheet(0).doRead();
    }


    private void packageTeacherInfo(Teacher teacher) {
        Dict dict = cmnClient.getDictById(teacher.getDepartmentId());
        teacher.setDepartmentName(dict.getName());
    }
}
