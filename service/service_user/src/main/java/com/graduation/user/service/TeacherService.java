package com.graduation.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.graduation.model.entity.Teacher;
import com.graduation.model.vo.ExcelContentVo;
import com.graduation.model.vo.SchoolInfoVo;
import com.graduation.model.vo.TeacherQueryVo;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tianwenle
 * @since 2022-11-16
 */
public interface TeacherService extends IService<Teacher> {

    Teacher getTeacherInfoById(Long id);

    List<Teacher> getTeacherList();

    Map<String,Object> teacherPage(Integer pageSize, Integer limit, TeacherQueryVo teacherQueryVo);

    List<SchoolInfoVo> getTeacherInfo();


    void batchAddStudents(MultipartFile file, ExcelContentVo excelContentVo) throws IOException;
}
