package com.graduation.user.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graduation.common.exception.ManageException;
import com.graduation.common.result.R;
import com.graduation.common.utils.JwtHelper;
import com.graduation.common.utils.MD5;
import com.graduation.model.entity.Administrator;
import com.graduation.model.entity.Student;
import com.graduation.model.entity.Teacher;
import com.graduation.model.entity.User;
import com.graduation.user.service.AdministratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tianwenle
 * @since 2022-11-16
 */
@RestController
@RequestMapping("/admin/user")
public class AdministratorController {

    @Autowired
    private AdministratorService administratorService;

    @GetMapping("/getCurrentUserInfo")
    public R getCurrentUserInfo(HttpServletRequest request) {

        String token = request.getHeader("X-Token");
        // 获取登录账号 查询用户的姓名
        String loginNo = JwtHelper.getLoginNo(token);

        QueryWrapper<Administrator> wrapper = new QueryWrapper<>();
        wrapper.eq("login_no", loginNo);

        Administrator administrator = administratorService.getOne(wrapper);

        if (administrator != null) {
            return R.ok().data("curUser", administrator);
        }

        return R.ok().data("name", "NULL");
    }


    @GetMapping("/getCountInfo")
    public R getCountInfo() {

        Map<String, Object> map = administratorService.getCountInfo();

        return R.ok().data(map);
    }


    @GetMapping("/getAdminEmail")
    public String getAdminEmail() {
        Administrator administrator = administratorService.getOne(null);
        return administrator.getEmail();
    }


    /**
     * 编写login请求接口
     */
    @PostMapping("/login")
    public R userLogin(@RequestBody Administrator administrator) {
        QueryWrapper<Administrator> wrapper = new QueryWrapper<>();
        // 这里的username是登录账户
        wrapper.eq("login_no", administrator.getUsername());
        wrapper.eq("password", MD5.encrypt(administrator.getPassword()));
        Administrator admin = administratorService.getOne(wrapper);
        if (admin == null) {
            return R.error().message("账号或密码错误！");
        }
        String token = JwtHelper.createToken(admin.getLoginNo(), admin.getUsername(), "admin");
        return R.ok().data("token", token);
    }

    /**
     * 编写info接口
     */
    @GetMapping("/info")
    public R infoShow(String token) {

        String loginNo = JwtHelper.getLoginNo(token);

        QueryWrapper<Administrator> wrapper = new QueryWrapper<>();
        wrapper.eq("login_no", loginNo);
        Administrator administratorServiceOne = administratorService.getOne(wrapper);
        return R.ok().data("roles", Arrays.asList("admin")).data("introduction", administratorServiceOne.getUsername()).data("avatar", administratorServiceOne.getHeadUrl()).data("name", administratorServiceOne.getNickName());
    }

    @PutMapping("/update")
    public R updateInfo(@RequestBody Administrator administrator, HttpServletRequest request) {
        String token = request.getHeader("X-Token");

        String loginNo = JwtHelper.getLoginNo(token);

        if (loginNo == null) {
            throw new ManageException("登录失效，请重新登录！");
        }
        QueryWrapper<Administrator> wrapper = new QueryWrapper<>();
        wrapper.eq("login_no", loginNo);
        Administrator administratorServiceOne = administratorService.getOne(wrapper);
        administrator.setId(administratorServiceOne.getId());
        administratorService.updateById(administrator);
        return R.ok();
    }

    @GetMapping("/getByUsername")
    public R getByUsername(HttpServletRequest request) {
        String token = request.getHeader("X-Token");

        String loginNo = JwtHelper.getLoginNo(token);

        if (loginNo == null) {
            throw new ManageException("登录失效，请重新登录！");
        }
        QueryWrapper<Administrator> wrapper = new QueryWrapper<>();
        wrapper.eq("login_no", loginNo);
        Administrator admin = administratorService.getOne(wrapper);
        return R.ok().data("admin", admin);
    }


}

