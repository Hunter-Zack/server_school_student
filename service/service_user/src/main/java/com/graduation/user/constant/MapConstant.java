 /*
  * Copyright (c) 2006, 2023, wuyahan 编写
  *
  */
 package com.graduation.user.constant;

 import java.util.Map;
 import java.util.concurrent.ConcurrentHashMap;

 /**
  * <p>Project: school end - MapConstant
  * <p>Powered by wuyahan On 2023-03-22 18:01:58
  *
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since 17
  */
 public class MapConstant {

     public static final Map<String,Object> MAP = new ConcurrentHashMap<>();

 }
