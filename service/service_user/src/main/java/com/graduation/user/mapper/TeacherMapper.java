package com.graduation.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.graduation.model.entity.Teacher;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author tianwenle
 * @since 2022-11-16
 */
public interface TeacherMapper extends BaseMapper<Teacher> {

}
