package com.graduation.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.graduation.model.entity.Administrator;

import java.util.Map;

public interface AdministratorService extends IService<Administrator> {

    Map<String, Object> getCountInfo();

}