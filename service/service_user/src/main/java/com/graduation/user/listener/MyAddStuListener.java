package com.graduation.user.listener;


import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.graduation.common.exception.ManageException;
import com.graduation.common.exception.TestException;
import com.graduation.model.entity.Student;
import com.graduation.model.vo.ExcelContentVo;
import com.graduation.model.vo.StudentExVo;

public class MyAddStuListener extends AnalysisEventListener<StudentExVo> {

    private BaseMapper<Student> baseMapper;

    private ExcelContentVo excelContentVo;

    public MyAddStuListener() {
    }

    public MyAddStuListener(BaseMapper<Student> baseMapper) {
        this.baseMapper = baseMapper;
    }

    public MyAddStuListener(BaseMapper<Student> baseMapper, ExcelContentVo excelContentVo) {
        this.baseMapper = baseMapper;
        this.excelContentVo = excelContentVo;
    }

    @Override
    public void invoke(StudentExVo studentExVo, AnalysisContext analysisContext) {
        System.out.println("studentExVo = " + studentExVo);
        Student student = new Student();
        // 添加学生登录账号以及学生姓名
        student.setLoginNo(studentExVo.getLoginNo());
        student.setUsername(studentExVo.getUsername());
        student.setDepartmentId(excelContentVo.getDepartmentId());
        student.setTeacherId(excelContentVo.getTeacherId());
        student.setUsername(studentExVo.getUsername());
        System.out.println("student = " + student);
        QueryWrapper<Student> wrapper = new QueryWrapper<>();
        wrapper.eq("login_no",studentExVo.getLoginNo());
        Student selectOne = baseMapper.selectOne(wrapper);
        if (selectOne == null){
            baseMapper.insert(student);
        }else {
//            throw new ManageException("检测到" + selectOne.getUsername() + "同学已存在，跳过当前同学添加");
            throw new TestException("检测到" + selectOne.getUsername() + "同学已存在，请检查后继续提交");
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }
}
