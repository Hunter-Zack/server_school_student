package com.graduation.work.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.graduation.model.entity.WorkInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author tianwenle
 * @since 2023-02-16
 */

public interface WorkInfoMapper extends BaseMapper<WorkInfo> {

}
