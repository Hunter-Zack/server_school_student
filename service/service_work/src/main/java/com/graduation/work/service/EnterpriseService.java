package com.graduation.work.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.graduation.model.entity.Enterprise;
import com.graduation.model.vo.EnterpriseVo;

import java.util.List;

/**
 * <p>
 * 企业信息表 服务类
 * </p>
 *
 * @author tianwenle
 * @since 2023-02-16
 */
public interface EnterpriseService extends IService<Enterprise> {

    List<EnterpriseVo> getAllEnvType();

    void insertEnv(Enterprise enterprise);

    Enterprise getInfo(String code);
}
