package com.graduation.work.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.graduation.model.entity.WorkInfo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tianwenle
 * @since 2023-02-16
 */
public interface WorkInfoService extends IService<WorkInfo> {

    void saveInfo(String studentName ,String workCode, List<String> time, String code,String position);

    boolean judgeStatusByUsername(String loginName);

    Map<String, Object> getStudentAndWorkInfoAndEnterpriseInfo(Long id);
}
