package com.graduation.work.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graduation.client.CmnClient;
import com.graduation.client.StudentClint;
import com.graduation.model.entity.*;
import com.graduation.model.vo.StudentWorkVo;
import com.graduation.rabbit.config.MQConst;
import com.graduation.rabbit.service.RabbitService;
import com.graduation.work.mapper.EnterpriseMapper;
import com.graduation.work.mapper.WorkInfoMapper;
import com.graduation.work.service.EnterpriseService;
import com.graduation.work.service.WorkInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tianwenle
 * @since 2023-02-16
 */
@Service
public class WorkInfoServiceImpl extends ServiceImpl<WorkInfoMapper, WorkInfo> implements WorkInfoService {

    @Autowired
    private EnterpriseService enterpriseService;

    @Autowired
    private StudentClint studentClint;

    @Autowired
    private CmnClient cmnClient;

    @Resource
    private EnterpriseMapper enterpriseMapper;

    @Autowired
    private RabbitService rabbitService;

    @Override
    public void saveInfo(String studentName, String workCode, List<String> time, String code,String position) {

        WorkInfo workInfo = new WorkInfo();
        // 根据学生的用户名查询用户的数据信息
        Long id = studentClint.getStudentIdByUsername(studentName);
        // 设置学生的id
        workInfo.setUserId(id);
        // 设置员工编号
        workInfo.setWorkCode(workCode);
        // 设置企业的统一信息码
        workInfo.setEnterpriseCode(code);
        // 设置工作的职位
        workInfo.setPosition(position);
        // 设置起始以及结束时间
        org.joda.time.LocalDate localDate = LocalDateTime.parse(Collections.min(time)).toLocalDate();
        LocalDate localDate1 = LocalDate.of(localDate.getYear(), localDate.getMonthOfYear(), localDate.getDayOfMonth());
        workInfo.setWorkStartTime(localDate1);

        localDate = LocalDateTime.parse(Collections.max(time)).toLocalDate();
        LocalDate localDate2 = LocalDate.of(localDate.getYear(), localDate.getMonthOfYear(), localDate.getDayOfMonth());
        workInfo.setWorkEndTime(localDate2);
        // 添加信息
        // 为了避免多次插入信息 直先判断是否存在 如果存在就先删除再添加
        QueryWrapper<WorkInfo> workInfoQueryWrapper = new QueryWrapper<>();
        workInfoQueryWrapper.eq("enterprise_code", code);
        WorkInfo info = baseMapper.selectOne(workInfoQueryWrapper);
        if (info != null) {
            baseMapper.deleteById(info);
        } else {
            // 添加工作信息
            baseMapper.insert(workInfo);
        }

    }

    @Override
    public boolean judgeStatusByUsername(String loginName) {
        // 获取当前用户的id
        Long studentId = studentClint.getStudentIdByUsername(loginName);
        // 判断用户在用户工作表内是否存在
        QueryWrapper<WorkInfo> workInfoQueryWrapper = new QueryWrapper<>();
        workInfoQueryWrapper.eq("user_id", studentId);
        WorkInfo workInfo = baseMapper.selectOne(workInfoQueryWrapper);

        if (workInfo == null) {
            return false;
        }
        // 获取企业信息编码 查看企业是否存在
        String enterpriseCode = workInfo.getEnterpriseCode();
        QueryWrapper<Enterprise> enterpriseQueryWrapper = new QueryWrapper<>();
        enterpriseQueryWrapper.eq("code", enterpriseCode);
        Enterprise enterprise = enterpriseMapper.selectOne(enterpriseQueryWrapper);
        // 不存在就返回false
        if (enterprise == null) {
            return false;
        }

        // 这里还需要一个操作 如果到这里都填写完毕 可以判定为实习已完成
        //  练习rabbitmq ||
        StudentWorkVo studentWorkVo = new StudentWorkVo();
        studentWorkVo.setId(studentId);
        studentWorkVo.setWork(true);
        rabbitService.save(MQConst.EXCHANGE_DIRECT_STUDENT,MQConst.ROUTING_STUDENT,studentWorkVo);

        return true;
    }

    @Override
    public Map<String, Object> getStudentAndWorkInfoAndEnterpriseInfo(Long id) {
        // 要返回的map集合
        Map<String, Object> map = new HashMap<>();

        // 根据学生id 查询学生的基本信息
        Student student = studentClint.getStudent(id);
        // 对学生信息进行封装
        packageStudentInfo(student);
        // 添加student对象的信息
        map.put("student",student);
        // 根据学生id 查询学生的工作信息
        QueryWrapper<WorkInfo> workInfoQueryWrapper = new QueryWrapper<>();
        workInfoQueryWrapper.eq("user_id",id);
        WorkInfo workInfo = baseMapper.selectOne(workInfoQueryWrapper);
        if (workInfo != null) {
            // work_info 没有什么好封装的直接返回即可
            map.put("workInfo", workInfo);
            // 根据统一社会信用编码添加企业的信息
            String enterpriseCode = workInfo.getEnterpriseCode();
            QueryWrapper<Enterprise> enterpriseQueryWrapper = new QueryWrapper<>();
            enterpriseQueryWrapper.eq("code", enterpriseCode);
            // 获取到企业信息
            Enterprise enterprise = enterpriseService.getOne(enterpriseQueryWrapper);
            // 企业信息需要简单封装一下类型
            String orgtype = enterprise.getOrgtype();
            Dict dict = cmnClient.getDictById(Long.valueOf(orgtype));
            // 封装企业信息
            enterprise.setEnvStr(dict.getName());
            // 添加企业信息
            map.put("enterprise", enterprise);
        }

        return map;
    }

    private void packageStudentInfo(Student student) {
        // 这里并不需要做什么判断了 当前用户提交了日志报告，那么必然是信息填写完整了
        Long teacherId = student.getTeacherId();
        // 获取指导老师名字 我感觉这里把老师的信息也展现出来比较好 方面管理员的一些通知 避免二次查询
        Teacher teacher = studentClint.getTeacherInfoByTeacherId(teacherId);
        // 学生的学院 班级 学年 级别 需要查询出来
        // 直接拼接到一起 方便展示
        // 学院id
        Long departmentId = student.getDepartmentId();
        Dict dept = cmnClient.getDictById(departmentId);
        // 班级级别
        Long classNo = student.getClassNo();
        Dict clazz = cmnClient.getDictById(classNo);
        // 学年
        Long year = student.getYear();
        Dict yearDict = cmnClient.getDictById(year);
        // 班级名称
        Long className = student.getClassName();
        Dict classnameDict = cmnClient.getDictById(className);
        // 填充班级信息 19级软件工程一班
        student.setFullInfo(yearDict.getName() + " " + classnameDict.getName() + " " + clazz.getName());
        // 学院还是单独存放比较好
        student.setDepartmentName(dept.getName());
        // 指导老师的信息
        // 老师的名字
        String username = teacher.getUsername();
        // 老师的手机号
        String phone = teacher.getPhone();
        // 老师的邮箱
        String email = teacher.getEmail();
        // 老师的头像
        String headUrl = teacher.getHeadUrl();
        // 填充教师的信息
        Map<String, String> stringInfo = new HashMap<>();

        stringInfo.put("username",username);
        stringInfo.put("phone",phone);
        stringInfo.put("email",email);
        stringInfo.put("headUrl",headUrl);
        // 这一步挺多余的
        student.setStringInfo(stringInfo);
    }
}
