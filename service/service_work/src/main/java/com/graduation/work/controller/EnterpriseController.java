package com.graduation.work.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graduation.common.result.R;
import com.graduation.model.entity.Enterprise;
import com.graduation.model.vo.EnterpriseVo;
import com.graduation.work.service.EnterpriseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 企业信息表 前端控制器
 * </p>
 *
 * @author tianwenle
 * @since 2023-02-16
 */
@RestController
@RequestMapping("/user/work/enterprise")
public class EnterpriseController {

    @Autowired
    private EnterpriseService enterpriseService;

    @GetMapping("/getAllEnvType")
    public R getAllEnvType(){

        return R.ok().data("list",enterpriseService.getAllEnvType());
    }

    @PostMapping("/insertEnvInfo")
    public R insertEnvInof(@RequestBody Enterprise enterprise){

        enterpriseService.insertEnv(enterprise);

        return R.ok();
    }

    @GetMapping("/getEnterpriseInfoByCode/{code}")
    public Enterprise getEnterpriseInfoByCode(@PathVariable("code") String code){

        return enterpriseService.getInfo(code);

    }


}

