 /*
  * Copyright (c) 2006, 2023, wuyahan 编写
  *
  */
 package com.graduation.work;

 import org.mybatis.spring.annotation.MapperScan;
 import org.springframework.boot.SpringApplication;
 import org.springframework.boot.autoconfigure.SpringBootApplication;
 import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
 import org.springframework.cloud.openfeign.EnableFeignClients;
 import org.springframework.context.annotation.ComponentScan;

 /**
  * <p>Project: manage - WorkApplication
  * <p>Powered by wuyahan On 2023-02-16 20:25:08
  *
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since 17
  */
@SpringBootApplication
@ComponentScan(basePackages = {"com.graduation"})
@MapperScan("com.graduation.work.mapper")
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.graduation")
 public class WorkApplication {
     public static void main(String[] args) {
         SpringApplication.run(WorkApplication.class,args);
     }
 }
