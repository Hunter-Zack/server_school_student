 /*
  * Copyright (c) 2006, 2023, wuyahan 编写
  *
  */
 package com.graduation.work.config;

 import com.graduation.rabbit.config.MQConst;
 import org.springframework.amqp.core.*;
 import org.springframework.beans.factory.annotation.Qualifier;
 import org.springframework.context.annotation.Bean;
 import org.springframework.context.annotation.Configuration;

 /**
  * <p>Project: school end - MQConfig
  * <p>Powered by wuyahan On 2023-02-18 17:08:28
  *
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since 17
  */
 @Configuration
 public class MQBindConfig {

     // 配置交换机
     @Bean
     @Qualifier("studentExchange")
     public Exchange studentExchange() {
         return ExchangeBuilder.directExchange(MQConst.EXCHANGE_DIRECT_STUDENT).durable(true).build();
     }

     // 配置队列
     @Bean
     @Qualifier("studentQueue")
     public Queue studentQueue(){
         return QueueBuilder.durable(MQConst.QUEUE_STUDENT).build();
     }

     // 绑定交换机
     @Bean
     public Binding binding(@Qualifier("studentQueue") Queue queue, @Qualifier("studentExchange") Exchange exchange){
         return BindingBuilder.bind(queue).to(exchange).with(MQConst.ROUTING_STUDENT).noargs();
     }

 }
