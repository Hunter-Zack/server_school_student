package com.graduation.work.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graduation.client.StudentClint;
import com.graduation.common.result.R;
import com.graduation.common.utils.JwtHelper;
import com.graduation.model.entity.InternshipLog;
import com.graduation.model.entity.WorkInfo;
import com.graduation.model.vo.WorkInfoTempVo;
import com.graduation.work.service.WorkInfoService;
import com.sun.corba.se.spi.orbutil.threadpool.Work;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tianwenle
 * @since 2023-02-16
 */
@RestController
@RequestMapping("/user/work/workInfo")
public class WorkInfoController {

    @Autowired
    private WorkInfoService workInfoService;

    @Autowired
    private StudentClint studentClint;


    @GetMapping("/judgeSaveEnpInfo")
    public R judgeSaveEnpInfo(HttpServletRequest request) {
        String token = request.getHeader("X-Token");
        String userName = JwtHelper.getUserName(token);
        // 判断当前对象是否存在
        Long studentId = studentClint.getStudentIdByUsername(userName);
        // 判断当前对象是否提交了报告
        QueryWrapper<WorkInfo> wrapper = new QueryWrapper<>();

        wrapper.eq("user_id", studentId);

        WorkInfo workInfo = workInfoService.getOne(wrapper);


        return R.ok().data("writeEnv", workInfo != null);
    }

    /**
     * 保存实习信息
     *
     * @param request
     * @param workInfoTempVo
     * @return
     */
    @PostMapping("/save")
    public R saveWorkInfo(
            // 获取当前用户的用户名
            HttpServletRequest request,
            @RequestBody WorkInfoTempVo workInfoTempVo

    ) {

        // 获取用户名
        String token = request.getHeader("X-Token");

        String loginNo = JwtHelper.getLoginNo(token);
        String workCode = workInfoTempVo.getWorkCode();
        List<String> time = workInfoTempVo.getTime();
        String code = workInfoTempVo.getCode();
        String position = workInfoTempVo.getPosition();
        // 保存工作信息
        workInfoService.saveInfo(loginNo, workCode, time, code, position);
        return R.ok();
    }

    /**
     * 判断用户是否填写了实习信息
     */
    @GetMapping("/judgeStatus")
    public R judgeStatus(HttpServletRequest request) {
        // 获取用户登录名
        String token = request.getHeader("X-Token");
        String loginNo = JwtHelper.getLoginNo(token);

        boolean b = workInfoService.judgeStatusByUsername(loginNo);
        return R.ok().data("status", b);
    }

    /**
     * 通过用户的id 查询个人信息、工作信息、企业信息
     *
     * @param id
     * @return
     */
    @GetMapping("/studentAndWorkInfoAndEnterpriseInfo/{id}")
    public R getFullDetailWorkInfo(@PathVariable("id") Long id) {

        Map<String, Object> map = workInfoService.getStudentAndWorkInfoAndEnterpriseInfo(id);

        return R.ok().data(map);
    }


    @GetMapping("/getStudentWorkInfo/{id}")
    public WorkInfo getStudentWorkInfo(@PathVariable("id") Long id){

        QueryWrapper<WorkInfo> wrapper = new QueryWrapper<>();

        wrapper.eq("user_id",id);

        return workInfoService.getOne(wrapper);

    }


}

