package com.graduation.work.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.graduation.model.entity.Enterprise;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 企业信息表 Mapper 接口
 * </p>
 *
 * @author tianwenle
 * @since 2023-02-16
 */

public interface EnterpriseMapper extends BaseMapper<Enterprise> {

}
