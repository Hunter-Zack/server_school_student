package com.graduation.log.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.graduation.model.entity.InternshipGrade;
import com.graduation.model.vo.GradeQueryVo;
import com.graduation.model.vo.LogAndGradeVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author tianwenle
 * @since 2022-11-18
 */
public interface InternshipGradeMapper extends BaseMapper<InternshipGrade> {

    public List<LogAndGradeVo> getPageListInfo(GradeQueryVo gradeQueryVo, @Param("pageSize") Integer pageSize,@Param("pageNum") Integer pageNum);

}
