package com.graduation.log.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.graduation.client.StudentClint;
import com.graduation.log.mapper.InternshipGradeMapper;
import com.graduation.log.service.InternshipGradeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.graduation.model.entity.InternshipGrade;
import com.graduation.model.entity.Student;
import com.graduation.model.enu.LogEnum;
import com.graduation.model.vo.GradeQueryVo;
import com.graduation.model.vo.LogAndGradeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tianwenle
 * @since 2022-11-18
 */
@Service
public class InternshipGradeServiceImpl extends ServiceImpl<InternshipGradeMapper, InternshipGrade> implements InternshipGradeService {

    @Autowired
    private StudentClint studentClint;

    @Override
    public Map<String, Object> pageList(Integer pageSize, Integer pageNum, GradeQueryVo gradeQueryVo) {
        List<LogAndGradeVo> pageListInfo = baseMapper.getPageListInfo(gradeQueryVo, pageSize , (pageNum - 1) * pageSize);
        for (LogAndGradeVo logAndGradeVo : pageListInfo) {
            this.packageStudent(logAndGradeVo);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("total",pageListInfo.size());
        map.put("list",pageListInfo);
        return map;
    }

    private void packageStudent(LogAndGradeVo logAndGradeVo) {
        Student student = studentClint.getStudent(logAndGradeVo.getStudentId());
        logAndGradeVo.setStudentName(student.getUsername());
        logAndGradeVo.setAuthString(LogEnum.getValueByStatus(logAndGradeVo.getStatus()));
    }


}
