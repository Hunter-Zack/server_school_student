package com.graduation.log.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.graduation.model.entity.InternshipLog;
import com.graduation.model.entity.Student;
import com.graduation.model.vo.GradeQueryVo;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tianwenle
 * @since 2022-11-18
 */
public interface InternshipLogService extends IService<InternshipLog> {

    Map<String, Object> pageList(Integer pageNum, Integer pageSize, GradeQueryVo gradeQueryVo);


    void upload(Student student);

    // 导出实习日志
    void exportData(HttpServletResponse response);
}
