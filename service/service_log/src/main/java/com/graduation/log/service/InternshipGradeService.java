package com.graduation.log.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.graduation.model.entity.InternshipGrade;
import com.graduation.model.vo.GradeQueryVo;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tianwenle
 * @since 2022-11-18
 */
public interface InternshipGradeService extends IService<InternshipGrade> {

    Map<String, Object> pageList(Integer pageSize, Integer pageNum, GradeQueryVo gradeQueryVo);
}
