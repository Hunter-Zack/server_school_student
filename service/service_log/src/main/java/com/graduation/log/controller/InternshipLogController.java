package com.graduation.log.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.graduation.client.StudentClint;
import com.graduation.common.exception.ManageException;
import com.graduation.common.result.R;
import com.graduation.common.utils.JwtHelper;
import com.graduation.log.service.InternshipLogService;
import com.graduation.model.entity.InternshipLog;
import com.graduation.model.entity.Student;
import com.graduation.model.entity.Teacher;
import com.graduation.model.vo.GradeQueryVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tianwenle
 * @since 2022-11-18
 */
@RestController
@RequestMapping("/admin/log/grade")
public class InternshipLogController {

    @Autowired
    private InternshipLogService internshipLogService;
    @Autowired
    private StudentClint studentClint;


    @GetMapping("/pageAll/{pageNum}/{pageSize}")
    public R getPageLogAll(@PathVariable Integer pageNum,
                           @PathVariable Integer pageSize,
                           GradeQueryVo gradeQueryVo,
                           HttpServletRequest request
    ) throws UnsupportedEncodingException {
        System.out.println("gradeQueryVo = " + gradeQueryVo);
        Map<String, Object> map = internshipLogService.pageList(pageNum, pageSize, gradeQueryVo);
        return R.ok().data(map);
    }

    @GetMapping("/page/{pageNum}/{pageSize}")
    public R getPageLogs(@PathVariable Integer pageNum,
                         @PathVariable Integer pageSize,
                         GradeQueryVo gradeQueryVo,
                         HttpServletRequest request
    ) throws UnsupportedEncodingException {
        String token = request.getHeader("X-Token");
        String loginNo = JwtHelper.getLoginNo(token);
        if (!studentClint.getStatus(loginNo)) {
            throw new ManageException("非法操作！");
        }
        Long id = studentClint.getTeacherIdByUsername(loginNo);
        if (id != null) {
            gradeQueryVo.setTeacherId(id);
        }
        System.out.println("gradeQueryVo = " + gradeQueryVo);
        Map<String, Object> map = internshipLogService.pageList(pageNum, pageSize, gradeQueryVo);
        return R.ok().data(map);
    }

    /**
     * 修改日志状态
     */
    @PutMapping("/changeStatus/{studentId}/{status}")
    public R changeStatus(
            @PathVariable Long studentId,
            @PathVariable Long status,
            @RequestParam(value = "comment", required = false) String comment,
            HttpServletRequest request) throws UnsupportedEncodingException {
        String token = request.getHeader("X-Token");
        String loginNo = JwtHelper.getLoginNo(token);

        if (!studentClint.getStatus(loginNo)) {
            throw new ManageException("非法操作！");
        }
        UpdateWrapper<InternshipLog> wrapper = new UpdateWrapper<>();
        wrapper.eq("student_id", studentId);
        wrapper.set("status", status);
        if (!StringUtils.isEmpty(comment)) {
            wrapper.set("comment", comment);
        }
        internshipLogService.update(wrapper);
        return R.ok();
    }

    @GetMapping("/getLogByStudentId/{id}")
    public InternshipLog getLogByStudentId(@PathVariable Long id) {
        QueryWrapper<InternshipLog> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", id);
        return internshipLogService.getOne(wrapper);
    }

    @PostMapping("/uploadLog")
    public R upload(@RequestBody Student student) {
        internshipLogService.upload(student);
        return R.ok();
    }

    @GetMapping("/export")
    public void exportData(HttpServletResponse response) throws IOException {
        internshipLogService.exportData(response);
    }


}

