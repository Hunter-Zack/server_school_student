package com.graduation.log.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.graduation.model.entity.InternshipLog;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author tianwenle
 * @since 2022-11-18
 */
public interface InternshipLogMapper extends BaseMapper<InternshipLog> {

}
