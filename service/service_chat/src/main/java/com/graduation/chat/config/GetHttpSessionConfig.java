package com.graduation.chat.config;

import com.graduation.chat.constant.RedisTemplateConstant;
import org.springframework.data.redis.core.RedisTemplate;

import javax.servlet.http.HttpSession;
import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;

public class GetHttpSessionConfig extends ServerEndpointConfig.Configurator {

    @Override
    public void modifyHandshake(ServerEndpointConfig sec, HandshakeRequest request, HandshakeResponse response) {
        RedisTemplate redisTemplate = new RedisTemplateConstant().getRedisTemplat();

        sec.getUserProperties().put(RedisTemplate.class.getName(),redisTemplate);

    }
}
