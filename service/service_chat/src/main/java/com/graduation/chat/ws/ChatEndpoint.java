package com.graduation.chat.ws;


import com.graduation.chat.constant.RedisTemplateConstant;
import com.graduation.utils.MessageUtils;
import com.graduation.utils.pojo.Message;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;


@Component
@ServerEndpoint(value = "/chat")
public class ChatEndpoint {


    private static final Map<String, Session> onlineUsers = new ConcurrentHashMap<>();


    /**
     * 建立websocket连接后，被调用
     *
     * @param session
     */
    @OnOpen
    public void onOpen(Session session, EndpointConfig config) {


        Jedis jedis = new Jedis("127.0.0.1", 6379);

        String ping = jedis.ping();

        System.out.println("ping = " + ping);

        String user = (String) jedis.get("user");


        jedis.close();

//        RedisTemplate redisTemplate = (RedisTemplate) config.getUserProperties().get(RedisTemplate.class.getName());
//
//
//        //1，将存储的名称获取到
//        String user = (String) redisTemplate.opsForValue().get("user");
        onlineUsers.put(user, session);
        //2，广播消息。需要将登陆的所有的用户推送给所有的用户
        String message = MessageUtils.getMessage(true, null, getFriends());
        broadcastAllUsers(message);
    }

    public Set getFriends() {
        Set<String> set = onlineUsers.keySet();
        return set;
    }

    private void broadcastAllUsers(String message) {
        try {
            //遍历map集合
            Set<Map.Entry<String, Session>> entries = onlineUsers.entrySet();
            for (Map.Entry<String, Session> entry : entries) {
                //获取到所有用户对应的session对象
                Session session = entry.getValue();
                //发送消息
                session.getBasicRemote().sendText(message);
            }
        } catch (Exception e) {
            //记录日志
        }
    }

    /**
     * 浏览器发送消息到服务端，该方法被调用
     * <p>
     * 张三  -->  李四
     *
     * @param message
     */
    @OnMessage
    public void onMessage(String message) {


        Jedis jedis = new Jedis("127.0.0.1", 6379);

        String ping = jedis.ping();

        System.out.println("ping = " + ping);

        String user = (String) jedis.get("user");


        jedis.close();

        try {
            //将消息推送给指定的用户
            Message msg = JSON.parseObject(message, Message.class);
            //获取 消息接收方的用户名
            String toName = msg.getToName();
            String mess = msg.getMessage();
            //获取消息接收方用户对象的session对象
            Session session = onlineUsers.get(toName);

            String msg1 = MessageUtils.getMessage(false, user, mess);
            session.getBasicRemote().sendText(msg1);
        } catch (Exception e) {
            //记录日志
        }
    }

    /**
     * 断开 websocket 连接时被调用
     *
     * @param session
     */
    @OnClose
    public void onClose(Session session) {


        //1,从onlineUsers中剔除当前用户的session对象

        Jedis jedis = new Jedis("127.0.0.1", 6379);

        String ping = jedis.ping();

        System.out.println("ping = " + ping);

        String user = (String) jedis.get("user");


        jedis.close();

        onlineUsers.remove(user);
        //2,通知其他所有的用户，当前用户下线了
        String message = MessageUtils.getMessage(true, null, getFriends());
        broadcastAllUsers(message);
    }


}
