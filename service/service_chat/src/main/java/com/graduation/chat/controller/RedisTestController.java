 /*
  * Copyright (c) 2006, 2023, wuyahan 编写
  *
  */
 package com.graduation.chat.controller;

 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.data.redis.core.RedisTemplate;
 import org.springframework.web.bind.annotation.GetMapping;
 import org.springframework.web.bind.annotation.RequestMapping;
 import org.springframework.web.bind.annotation.RestController;

 /**
  * <p>Project: school end - RedisTestController
  * <p>Powered by wuyahan On 2023-03-23 17:36:45
  *
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since 17
  */
 @RestController
 @RequestMapping("/redis")
 public class RedisTestController {

     @Autowired
     private RedisTemplate redisTemplate;

     @GetMapping("/test")
     public String getString() {
         String user = (String) redisTemplate.opsForValue().get("user");
         return user;
     }

 }
