 /*
  * Copyright (c) 2006, 2023, wuyahan 编写
  *
  */
 package com.graduation.chat.constant;

 import org.springframework.beans.BeansException;
 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.context.ApplicationContext;
 import org.springframework.context.ApplicationContextAware;
 import org.springframework.context.annotation.ComponentScan;
 import org.springframework.data.redis.core.RedisTemplate;
 import org.springframework.stereotype.Component;

 /**
  * <p>Project: school end - RedisTemplateConstant
  * <p>Powered by wuyahan On 2023-03-23 17:45:00
  *
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since 17
  */
 @Component
 public class RedisTemplateConstant {

     @Autowired
     private RedisTemplate redisTemplate;


     public RedisTemplate getRedisTemplat() {
         return this.redisTemplate;
     }

     public void setRedisTemplate(RedisTemplate redisTemplate) {
         this.redisTemplate = redisTemplate;
     }
 }
