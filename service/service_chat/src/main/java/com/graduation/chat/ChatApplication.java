 /*
  * Copyright (c) 2006, 2023, wuyahan 编写
  *
  */
 package com.graduation.chat;

 import com.graduation.chat.constant.RedisTemplateConstant;
 import com.graduation.chat.ws.ChatEndpoint;
 import org.springframework.boot.SpringApplication;
 import org.springframework.boot.autoconfigure.SpringBootApplication;
 import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
 import org.springframework.cloud.openfeign.EnableFeignClients;
 import org.springframework.context.ConfigurableApplicationContext;
 import org.springframework.context.annotation.ComponentScan;
 import org.springframework.data.redis.core.RedisTemplate;
 import org.springframework.data.redis.core.StringRedisTemplate;

 /**
  * <p>Project: school end - ChatApplication
  * <p>Powered by wuyahan On 2023-03-22 16:04:40
  *
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since 17
  */

 @SpringBootApplication
@ComponentScan(basePackages = {"com.graduation"})
 @EnableDiscoveryClient
 @EnableFeignClients(basePackages = "com.graduation")
 public class ChatApplication {

     public static void main(String[] args) {
         ConfigurableApplicationContext context = SpringApplication.run(ChatApplication.class, args);

         ChatEndpoint bean = context.getBean(ChatEndpoint.class);
         System.out.println("bean = " + bean);

         RedisTemplateConstant templateConstant = context.getBean(RedisTemplateConstant.class);

         RedisTemplate redisTemplat = templateConstant.getRedisTemplat();

         System.out.println("redisTemplat = " + redisTemplat);

         templateConstant.setRedisTemplate(redisTemplat);

     }

 }
