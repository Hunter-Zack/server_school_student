package com.graduation.cmn;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.graduation")
@MapperScan("com.graduation.cmn.mapper")
@EnableDiscoveryClient
public class CmnApplication {

    public static void main(String[] args) {
        SpringApplication.run(CmnApplication.class,args);
    }
}
