package com.graduation.cmn.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.graduation.model.entity.Dict;

/**
 * <p>
 * 组织架构表 Mapper 接口
 * </p>
 *
 * @author tianwenle
 * @since 2022-11-15
 */
public interface DictMapper extends BaseMapper<Dict> {

}
