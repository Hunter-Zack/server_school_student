package com.graduation.cmn.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.graduation.cmn.mapper.DictMapper;
import com.graduation.model.entity.Dict;
import com.graduation.model.vo.DictEeVo;
import org.springframework.beans.BeanUtils;

public class ExcelListener extends AnalysisEventListener<DictEeVo> {

    protected DictMapper dictMapper;

    public ExcelListener(DictMapper dictMapper){
        this.dictMapper = dictMapper;
    }

    @Override
    public void invoke(DictEeVo dictEeVo, AnalysisContext analysisContext) {
        System.out.println("dictEeVo = " + dictEeVo);
        Dict dict = new Dict();
        BeanUtils.copyProperties(dictEeVo,dict);
        System.out.println("dict = " + dict);
        System.out.println("dictEeVo = " + dictEeVo);
        // 如果数据库不存在 就添加 存在就更新
        Dict selectById = dictMapper.selectById(dict.getId());
        if (selectById == null){
            dictMapper.insert(dict);
        }else {
            dictMapper.updateById(dict);
        }

    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }
}
