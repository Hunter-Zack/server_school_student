package com.graduation.cmn.service.impl;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.graduation.cmn.listener.ExcelListener;
import com.graduation.cmn.mapper.DictMapper;
import com.graduation.cmn.service.DictService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.graduation.model.entity.Dict;
import com.graduation.model.vo.DictEeVo;
import com.graduation.model.vo.SchoolInfoVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 组织架构表 服务实现类
 * </p>
 *
 * @author tianwenle
 * @since 2022-11-15
 */
@Service
public class DictServiceImpl extends ServiceImpl<DictMapper, Dict> implements DictService {



    /**
     * 写数据
     *
     * @param response
     * @throws IOException
     */
    @Override
    public void exportData(HttpServletResponse response) throws IOException {
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        String fileName = URLEncoder.encode("数据字典", "UTF-8");
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");

        List<Dict> dicts = baseMapper.selectList(null);
        List<DictEeVo> eeVos = new ArrayList<>();
        for (Dict dict : dicts) {
            DictEeVo dictEeVo = new DictEeVo();
            BeanUtils.copyProperties(dict, dictEeVo);
            eeVos.add(dictEeVo);
        }
        EasyExcel.write(response.getOutputStream(), DictEeVo.class).sheet("数据字典").doWrite(eeVos);

    }

    @Override
    public void upload(MultipartFile file) throws IOException {
        // 文件上传后 进行解析文件
        EasyExcel.read(file.getInputStream(), DictEeVo.class, new ExcelListener(baseMapper)).sheet(0).doRead();
    }

    @Override
    public List<Dict> getPackageData(Long id) {

        QueryWrapper<Dict> wrapper = new QueryWrapper<>();
        wrapper.eq("parent_id", id);
        List<Dict> dictList = baseMapper.selectList(wrapper);
        for (Dict dict : dictList) {
            if (hasChild(dict)) {
                dict.setHasChildren(true);
            } else {
                dict.setHasChildren(false);
            }
        }
        return dictList;
    }

    @Override
    public List<SchoolInfoVo> getClassAndValueInfo() {
        QueryWrapper<Dict> wrapper = new QueryWrapper<>();
        wrapper.eq("parent_id", 666); // 获取所有学院的数据
        List<Dict> dicts = baseMapper.selectList(wrapper);
        List<SchoolInfoVo> list = new ArrayList<>();
        for (Dict dict : dicts) {
            SchoolInfoVo schoolInfoVo = new SchoolInfoVo();
            schoolInfoVo.setLabel(dict.getName());
            schoolInfoVo.setValue(dict.getValue());
            list.add(schoolInfoVo);
        }
        return list;
    }

    @Override
    public List<SchoolInfoVo> getYearInfo() {
        QueryWrapper<Dict> wrapper = new QueryWrapper<>();
        wrapper.eq("parent_id", 6666); // 获取所有学年
        List<Dict> dicts = baseMapper.selectList(wrapper);
        List<SchoolInfoVo> list = new ArrayList<>();
        for (Dict dict : dicts) {
            SchoolInfoVo schoolInfoVo = new SchoolInfoVo();
            schoolInfoVo.setLabel(dict.getName());
            schoolInfoVo.setValue(dict.getValue());
            list.add(schoolInfoVo);
        }
        return list;
    }

    @Override
    public List<SchoolInfoVo> getClassNameInfo(Long id) {
        QueryWrapper<Dict> wrapper = new QueryWrapper<>();
        wrapper.eq("parent_id", id); // 获取当前学院下的班级信息
        List<Dict> dicts = baseMapper.selectList(wrapper);
        List<SchoolInfoVo> list = new ArrayList<>();
        for (Dict dict : dicts) {
            SchoolInfoVo schoolInfoVo = new SchoolInfoVo();
            schoolInfoVo.setLabel(dict.getName());
            schoolInfoVo.setValue(dict.getValue());
            list.add(schoolInfoVo);
        }
        return list;
    }

    @Override
    public List<SchoolInfoVo> getClassNoInfo() {
        QueryWrapper<Dict> wrapper = new QueryWrapper<>();
        wrapper.eq("parent_id", 3333); // 获取所有班级No
        List<Dict> dicts = baseMapper.selectList(wrapper);
        List<SchoolInfoVo> list = new ArrayList<>();
        for (Dict dict : dicts) {
            SchoolInfoVo schoolInfoVo = new SchoolInfoVo();
            schoolInfoVo.setLabel(dict.getName());
            schoolInfoVo.setValue(dict.getValue());
            list.add(schoolInfoVo);
        }
        return list;
    }

    @Override
    public List<Dict> getEnvInfoValueAndName(Long id) {
        // 获取查询器
        QueryWrapper<Dict> wrapper = new QueryWrapper<>();
        // 设置查询id
        wrapper.eq("parent_id",id);
        // 查询数据
        List<Dict> dicts = baseMapper.selectList(wrapper);
        return dicts;
    }


//    private SchoolInfoVo setChild(Dict dict) {
////        SchoolInfoVo schoolInfoVo = new SchoolInfoVo();
////        schoolInfoVo.setLabel(dict.getName());
////        schoolInfoVo.setValue(dict.getValue());
//////        if (hasChild(dict)) {
////            QueryWrapper<Dict> wrapper = new QueryWrapper<>();
////            wrapper.eq("parent_id", dict.getId());
////            List<Dict> dicts = baseMapper.selectList(wrapper);
////            List<SchoolInfoVo> list = new ArrayList<SchoolInfoVo>();
////            SchoolInfoVo schoolInfoVo1 = new SchoolInfoVo();
////            for (Dict dict1 : dicts) {
//////                schoolInfoVo.setChildren(setChild(dict1));
////
////                schoolInfoVo1.setLabel(dict1.getName());
////                schoolInfoVo1.setValue(dict1.getValue());
////
////                list.add(setChild(dict1));
////            }
////            schoolInfoVo1.setChildren(list);
//////        }else {
//////            schoolInfoVo.setChildren(null);
//////        }
////            return schoolInfoVo;
//        return null;
//    }

    private boolean hasChild(Dict dict) {
        QueryWrapper<Dict> wrapper = new QueryWrapper<>();
        wrapper.eq("parent_id", dict.getId());
        Integer integer = baseMapper.selectCount(wrapper);
        return integer > 0;
    }


}
