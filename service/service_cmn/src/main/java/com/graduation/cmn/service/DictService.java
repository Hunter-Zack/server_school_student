package com.graduation.cmn.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.graduation.model.entity.Dict;
import com.graduation.model.vo.SchoolInfoVo;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 * 组织架构表 服务类
 * </p>
 *
 * @author tianwenle
 * @since 2022-11-15
 */
public interface DictService extends IService<Dict> {

    void exportData(HttpServletResponse response) throws IOException;

    void upload(MultipartFile file) throws IOException;

    List<Dict> getPackageData(Long id);

    List<SchoolInfoVo> getClassAndValueInfo();

    List<SchoolInfoVo> getYearInfo();

    List<SchoolInfoVo> getClassNameInfo(Long id);

    List<SchoolInfoVo> getClassNoInfo();


    List<Dict> getEnvInfoValueAndName(Long id);
}
