package com.graduation.cmn.controller;


import com.graduation.cmn.service.DictService;
import com.graduation.common.result.R;
import com.graduation.model.entity.Dict;
import com.graduation.model.vo.SchoolInfoVo;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 * 组织架构表 前端控制器
 * </p>
 *
 * @author tianwenle
 * @since 2022-11-15
 */
@RestController
@RequestMapping("/admin/dict")
@Api(value = "字典接口", tags = "数据字典相关的接口", description = "共同数据存放")
public class DictController {

    @Autowired
    private DictService dictService;

    @GetMapping("/export")
    public void exportData(HttpServletResponse response) throws IOException {
        dictService.exportData(response);
    }

    @PostMapping("/upload")
    public R upload(MultipartFile file) throws IOException {
        dictService.upload(file);
        return R.ok();
    }


    @GetMapping("/getAllListByParentId/{id}")
    public List<Dict> getAllDataType(@PathVariable("id") Long id) {
        return dictService.getEnvInfoValueAndName(id);
    }

    @GetMapping("/getList/{id}")
    public R getAllData(@PathVariable("id") Long id) {
        List<Dict> list = dictService.getPackageData(id);
        return R.ok().data("list", list);
    }

    @GetMapping("/getDictById/{id}")
    public Dict getDictById(@PathVariable("id") Long id) {
        return dictService.getById(id);
    }


    @GetMapping("/getClassInfo")
    public R getClassInfo() {
        List<SchoolInfoVo> list = dictService.getClassAndValueInfo();
        return R.ok().data("list", list);
    }

    @GetMapping("/getYearInfo")
    public R getYearInfo() {
        List<SchoolInfoVo> list = dictService.getYearInfo();
        return R.ok().data("list", list);
    }

    @GetMapping("/getClassNameInfo/{id}")
    public R getClassNameInfo(@PathVariable Long id) {
        List<SchoolInfoVo> list = dictService.getClassNameInfo(id);
        return R.ok().data("list", list);
    }

    @GetMapping("/getClassNoInfo")
    public R getClassNoInfo() {
        List<SchoolInfoVo> list = dictService.getClassNoInfo();
        return R.ok().data("list", list);
    }


}

