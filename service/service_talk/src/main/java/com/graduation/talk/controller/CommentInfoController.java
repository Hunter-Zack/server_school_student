package com.graduation.talk.controller;


import com.alibaba.fastjson.JSONObject;
import com.graduation.common.result.R;
import com.graduation.model.entity.CommentInfo;
import com.graduation.model.vo.BlogVo;
import com.graduation.talk.service.CommentInfoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * <p>
 * 前端控制器
 * </p>
 * 讨论区
 *
 * @author tianwenle
 * @since 2023-03-08
 */
@RestController
@RequestMapping("/user/talk/commentInfo")
public class CommentInfoController {

    @Autowired
    private RedisTemplate redisTemplate;

    @PostMapping("/save/{id}")
    public R save(@RequestBody String content, @PathVariable String id) {

        try {
            String decode = URLDecoder.decode(content, "UTF-8");

            System.out.println("decode = " + decode);

            redisTemplate.opsForValue().set(id, decode);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }


        return R.ok();
    }

    @GetMapping("/get/{id}")
    public R getCommonsById(@PathVariable String id) {

        String content = null;
        try {
            content = redisTemplate.opsForValue().get(id).toString();

            content = content.substring(0, content.length() - 1);

            return R.ok().data("content", content);


        } catch (Exception e) {

            return R.ok().data("content", "[]");
        }


    }


}

