package com.graduation.talk.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.graduation.model.entity.CommentInfo;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author tianwenle
 * @since 2023-03-08
 */
public interface CommentInfoMapper extends BaseMapper<CommentInfo> {

}
