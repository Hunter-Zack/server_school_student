package com.graduation.talk.service.impl;

import com.graduation.client.CmnClient;
import com.graduation.client.StudentClint;
import com.graduation.model.entity.*;
import com.graduation.model.enu.WorkStatusEnum;
import com.graduation.talk.mapper.DiscussionAreaMapper;
import com.graduation.talk.service.DiscussionAreaService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tianwenle
 * @since 2023-03-08
 */
@Service
public class DiscussionAreaServiceImpl extends ServiceImpl<DiscussionAreaMapper, DiscussionArea> implements DiscussionAreaService {


    @Autowired
    private StudentClint studentClint;

    @Autowired
    private CmnClient cmnClient;

    @Override
    public Map<String, Object> getDetailAboutDiscuss(Long id) {
        // 获取文章内容
        DiscussionArea discussionArea = baseMapper.selectById(id);

        // 获取用户信息
        Long userId = discussionArea.getUserId();
        // 封装学生信息
        Student student = studentClint.getStudent(userId);
        discussionArea.setUsername(student.getUsername());

        // 封装后的信息
        Map<String, Object> map = new HashMap<>();
        map.put("discuss", discussionArea);
        packageStudent(student);
        map.put("user", student);

        return map;
    }

    private void packageStudent(Student student) {
        if (student.getYear() == null || student.getClassName() == null || student.getClassNo() == null || student.getYear() == 0 || student.getClassName() == 0 || student.getClassNo() == 0 || student.getDepartmentId() == null || student.getTeacherId() == null) {
            // 这里还不能直接进行返回 避免后续读取到这里读取不到信息，必须对stringInfo进行包装
            Map<String, String> map = new HashMap<>();
            // 如果省事 可以直接填充假的数据
            if (student.getYear() == null || student.getYear() == 0) {
                map.put("year", "");
            }
            if (student.getClassName() == null || student.getClassName() == 0) {
                map.put("classname", "");
            }
            if (student.getClassNo() == null || student.getClassNo() == 0) {
                map.put("classno", "");
            }
            if (student.getDepartmentId() == null || student.getDepartmentId() == 0) {
                map.put("dept", "");
            }
            if (student.getTeacherId() == null || student.getTeacherId() == 0) {
                // 添加指导老师的姓名
                map.put("teacherName", "");
            }
            // 存储数据
            student.setStringInfo(map);

            return;
        }
        Dict dictYear = cmnClient.getDictById(student.getYear());
        Dict className = cmnClient.getDictById(student.getClassName());
        Dict dictClasNo = cmnClient.getDictById(student.getClassNo());
        student.setFullInfo(dictYear.getName() + className.getName() + dictClasNo.getName());
        Dict dep = cmnClient.getDictById(student.getDepartmentId());
        student.setDepartmentName(dep.getName());

        Long teacherId = student.getTeacherId();
        String teacherName = studentClint.getTeacherNameById(teacherId);
        // 将字符串信息传入到执行存储的map集合中
        Map<String, String> map = new HashMap<>();
        map.put("year", dictYear.getName());
        map.put("classname", className.getName());
        map.put("classno", dictClasNo.getName());
        map.put("dept", dep.getName());
        // 添加指导老师的姓名
        map.put("teacherName", teacherName);
        // 存储数据
        student.setStringInfo(map);


    }
}
