package com.graduation.talk.service.impl;

import com.graduation.model.entity.CommentInfo;
import com.graduation.talk.mapper.CommentInfoMapper;
import com.graduation.talk.service.CommentInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tianwenle
 * @since 2023-03-08
 */
@Service
public class CommentInfoServiceImpl extends ServiceImpl<CommentInfoMapper, CommentInfo> implements CommentInfoService {

}
