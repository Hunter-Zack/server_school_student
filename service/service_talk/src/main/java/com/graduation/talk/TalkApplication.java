 /*
  * Copyright (c) 2006, 2023, wuyahan 编写
  *
  */
 package com.graduation.talk;

 import org.mybatis.spring.annotation.MapperScan;
 import org.springframework.boot.SpringApplication;
 import org.springframework.boot.autoconfigure.SpringBootApplication;
 import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
 import org.springframework.cloud.openfeign.EnableFeignClients;
 import org.springframework.context.annotation.ComponentScan;

 /**
  * <p>Project: school end - TalkApplication
  * <p>Powered by wuyahan On 2023-03-08 21:01:44
  *
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since 17
  */
 @SpringBootApplication
 @ComponentScan(basePackages = {"com.graduation"})
 @MapperScan("com.graduation.talk.mapper")
 @EnableDiscoveryClient
 @EnableFeignClients(basePackages = "com.graduation")
 public class TalkApplication {
    public static void main(String[] args) {
         SpringApplication.run(TalkApplication.class,args);
     }

 }
