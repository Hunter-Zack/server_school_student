package com.graduation.talk.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.graduation.model.entity.CommentInfo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tianwenle
 * @since 2023-03-08
 */
public interface CommentInfoService extends IService<CommentInfo> {

}
