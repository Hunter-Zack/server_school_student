package com.graduation.talk.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.graduation.model.entity.DiscussionArea;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tianwenle
 * @since 2023-03-08
 */
public interface DiscussionAreaService extends IService<DiscussionArea> {

    Map<String, Object> getDetailAboutDiscuss(Long id);
}
