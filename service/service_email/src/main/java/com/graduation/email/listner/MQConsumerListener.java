 /*
  * Copyright (c) 2006, 2023, wuyahan 编写
  *
  */
 package com.graduation.email.listner;

 import com.graduation.client.StudentClint;
 import com.graduation.email.util.EmailUtil;
 import com.graduation.model.vo.ReportDiscussVo;
 import com.graduation.rabbit.config.MQConst;
 import org.springframework.amqp.rabbit.annotation.Exchange;
 import org.springframework.amqp.rabbit.annotation.Queue;
 import org.springframework.amqp.rabbit.annotation.QueueBinding;
 import org.springframework.amqp.rabbit.annotation.RabbitListener;
 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.stereotype.Component;

 /**
  * <p>Project: school end - MQConsumerListner
  * <p>Powered by wuyahan On 2023-02-18 17:18:55
  *
  * @author wuyahan [tianwenle2000@163.com]
  * @version 1.0
  * @since 17
  */
 @Component
 public class MQConsumerListener {

     @Autowired
     private EmailUtil emailUtil;

     @Autowired
     private StudentClint studentClint;


     @RabbitListener(
             bindings = @QueueBinding(
                     value = @Queue(MQConst.QUEUE_EMAIL),
                     exchange = @Exchange(MQConst.EXCHANGE_DIRECT_EMAIL),
                     key = MQConst.ROUTING_EMAIL
             )
     )
     public void consumer(ReportDiscussVo reportDiscussVo){
         // 获取管理员的邮箱
         String adminEmail = studentClint.getAdminEmail();
         // 拼接举报信
         String url = reportDiscussVo.getUrl();
         String content = reportDiscussVo.getContent();

         String msg = "被举报的文章地址：" + url + "; 用户举报说明： " + content;

         emailUtil.sendMessage(adminEmail,"您收到了一封举报信",msg);

     }


 }
