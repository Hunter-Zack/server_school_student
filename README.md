# 项目介绍
> 本项目为一个分布式项目，采用SpringCloud Alibaba的各种组件完成。

## 技术栈
**除了SpringCloud Alibaba各种组件外，还采用了如下组件：**
- Redis进行一些数据的缓存
- RabbitMQ进行跨服务通信
- MongoDB进行评论区存储

## 项目启动 
1. 修改项目中对应的地址 启动Nacos【端口号8848】
2. 修改项目中对应的地址 启动redis数据库
3. 修改项目中对应的地址 启动RabbitMQ
4. 修改service-oss服务中的阿里云oss密钥

> 启动GateWayApplication以及service包下的所有服务
