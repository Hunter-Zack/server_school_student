package com.graduation.client;

import com.graduation.model.entity.Enterprise;
import com.graduation.model.entity.WorkInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * <p>Project: school end - WorkClient
 * <p>Powered by wuyahan On 2023-05-04 10:53:53
 *
 * @author wuyahan [tianwenle2000@163.com]
 * @version 1.0
 * @since 17
 */
@FeignClient("service-work")
public interface WorkClient {

    @GetMapping("/user/work/enterprise/getEnterpriseInfoByCode/{code}")
    public Enterprise getEnterpriseInfoByCode(@PathVariable("code") String code);

    @GetMapping("/user/work/workInfo/getStudentWorkInfo/{id}")
    public WorkInfo getStudentWorkInfo(@PathVariable("id") Long id);

}
