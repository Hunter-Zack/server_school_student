package com.graduation.client;

import com.graduation.model.entity.Administrator;
import com.graduation.model.entity.Student;
import com.graduation.model.entity.Teacher;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Map;

@FeignClient("service-user")
public interface StudentClint {

    @GetMapping("/admin/user/getAdminEmail")
    public String getAdminEmail();

    // 获取老师的信息
    @GetMapping("/admin/user/teacher/getTeacherInfoByTeacherId/{id}")
    public Teacher getTeacherInfoByTeacherId(@PathVariable("id") Long id);


    // 获取学生编号 根据学生的id
    @GetMapping("/admin/user/student/getStudentIdByUsername/{name}")
    public Long getStudentIdByUsername(@PathVariable("name") String name);

    @GetMapping("/admin/user/student/getStudentById/{id}")
    public Student getStudent(@PathVariable("id") Long id);

    @GetMapping("/admin/user/teacher/getStatus/{loginNo}")
    public Boolean getStatus(@PathVariable String loginNo);

    @GetMapping("/admin/user/teacher/getTeacherIdByLoginNo/{loginNo}")
    public Long getTeacherIdByUsername(@PathVariable String loginNo);

    @GetMapping("/admin/user/teacher/getNameById/{id}")
    public String getTeacherNameById(@PathVariable("id") Long id);

    @GetMapping("/admin/user/base/getMap")
    public Map getCommonMap();


    @GetMapping("/admin/user/student/getFullStudentInfoById/{id}")
    public Student getFullStudentInfoById(@PathVariable("id") Long id);

}
