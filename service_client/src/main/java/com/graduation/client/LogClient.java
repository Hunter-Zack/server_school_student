package com.graduation.client;

import com.graduation.model.entity.InternshipGrade;
import com.graduation.model.entity.InternshipLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("service-log")
public interface LogClient {
    // 判断当前学生是否上传了日志
    @GetMapping("/admin/log/grade/getLogByStudentId/{id}")
    public InternshipLog getLogByStudentId(@PathVariable Long id);

    // 获取指定实习日志的评分记录
    @GetMapping("/admin/log/getGradeByLogId/{id}")
    public InternshipGrade getGradeByLogId(@PathVariable Long id);

}
