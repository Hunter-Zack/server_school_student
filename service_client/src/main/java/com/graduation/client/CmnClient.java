package com.graduation.client;

import com.graduation.model.entity.Dict;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient("service-cmn")
public interface CmnClient {

    @GetMapping("/admin/dict/getDictById/{id}")
    public Dict getDictById(@PathVariable("id") Long id);

    @GetMapping("/admin/dict/getAllListByParentId/{id}")
    public List<Dict> getAllDataType(@PathVariable("id") Long id);
}
